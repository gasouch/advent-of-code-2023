# Advent Of Code 2023

Mes implem en `Java 21` de [l'Advent Of Code 2023](https://adventofcode.com/2023)

(Mon projet 2022 est aussi accessible [ici](https://gitlab.com/gasouch/advent-of-code-2022))

## Prérequis

Il faut compiler les classes utilitaires utilisées dans les exercices : 

```shell
javac src/main/java/utils/*
```

## Les exercices

Pour tester les exercices il faut se placer dans le répertoire `src/main/java` puis exécuter la commande suivante (exemple pour le day1) :

```shell
❯ java day1/Main.java # or day2/Main.java ;-) 
Running Day 1 🚀
Response for stage 1 must be : 55172
Response for stage 1 must be : 54925
```
