package day9;

import utils.InputFileUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static utils.ConsoleUtils.ANSI_GREEN;
import static utils.ConsoleUtils.ANSI_RESET;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 9 🚀");
        var lines = InputFileUtils.extraction("day9/input.txt", "\n");
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage(lines) + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage(lines) + ANSI_RESET);
    }

    static int firstStage(List<String> lines) {
        return lines.stream().mapToInt(line -> {
            var sequences = prepareSequences(line);
            for (int i = sequences.size() - 2; i >= 0; i--) {
                sequences.get(i).add(sequences.get(i).getLast() + sequences.get(i + 1).getLast());
            }
            return sequences.get(0).getLast();
        }).sum();
    }

    static int secondStage(List<String> lines) {
        return lines.stream().mapToInt(line -> {
            var sequences = prepareSequences(line);
            for (int i = sequences.size() - 2; i >= 0; i--) {
                sequences.get(i).add(0, sequences.get(i).getFirst() - sequences.get(i + 1).getFirst());
            }
            return sequences.get(0).getFirst();
        }).sum();
    }

    static ArrayList<List<Integer>> prepareSequences(String line) {
        var sequences = new ArrayList<List<Integer>>();
        sequences.add(Arrays.stream(line.split("\\s")).map(Integer::parseInt).collect(Collectors.toList()));
        var notTheLastSequence = sequences.get(0).stream().anyMatch(i -> i != 0);
        var currentSequence = sequences.get(0);
        while (notTheLastSequence) {
            currentSequence = calculateNextSequence(currentSequence);
            sequences.add(currentSequence);
            notTheLastSequence = currentSequence.stream().anyMatch(i -> i != 0);
        }
        return sequences;
    }

    private static List<Integer> calculateNextSequence(List<Integer> sequence) {
        return IntStream.range(0, sequence.size() - 1)
                .mapToObj(i -> sequence.get(i + 1) - sequence.get(i))
                .collect(Collectors.toList());
    }
}