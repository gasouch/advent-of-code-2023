package day11;

import utils.InputFileUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static utils.ConsoleUtils.ANSI_GREEN;
import static utils.ConsoleUtils.ANSI_RESET;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 11 🚀");
        var lines = InputFileUtils.extraction("day11/input.txt", "\n");
        universeHeight = (long) lines.size();
        universeWidth = (long) lines.getFirst().length();
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage(lines) + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage(lines) + ANSI_RESET);
    }

    static BigInteger firstStage(List<String> lines) {
        initGalaxies(lines);
        expandUniverse(1);
        calculateDistances();
        final AtomicReference<BigInteger> sum = new AtomicReference<>(BigInteger.ZERO);
        galaxies.forEach(g -> g.distancesTo.values().forEach(distance -> sum.accumulateAndGet(distance, BigInteger::add)));
        return sum.get().divide(BigInteger.TWO);
    }

    static BigInteger secondStage(List<String> lines) {
        initGalaxies(lines);
        expandUniverse(999999);
        calculateDistances();
        final AtomicReference<BigInteger> sum = new AtomicReference<>(BigInteger.ZERO);
        galaxies.forEach(g -> g.distancesTo.values().forEach(distance -> sum.accumulateAndGet(distance, BigInteger::add)));
        return sum.get().divide(BigInteger.TWO);
    }

    static void initGalaxies(List<String> lines) {
        galaxies=new ArrayList<>();
        for (int y = 0; y < lines.size(); y++) {
            var chars = lines.get(y).toCharArray();
            for (int x = 0; x < chars.length; x++) {
                if ("#".equals(String.valueOf(chars[x]))) {
                    galaxies.add(new Galaxy(BigInteger.valueOf(x), BigInteger.valueOf(y)));
                }
            }
        }
    }

    static void expandUniverse(int factor) {
        var galaxiesX = galaxies.stream().map(g -> g.x.intValue()).toList();
        var galaxiesY = galaxies.stream().map(g -> g.y.intValue()).toList();
        var missingLines = new ArrayList<Integer>();
        var missingColumns = new ArrayList<Integer>();
        for (int x = 0; x < universeWidth; x++) {
            if (!galaxiesX.contains(x)) {
                missingColumns.add(x);
            }
        }
        for (int y = 0; y < universeHeight; y++) {
            if (!galaxiesY.contains(y)) {
                missingLines.add(y);
            }
        }
        var galaxiesToMoveX = new ArrayList<Galaxy>();
        missingColumns.forEach(x -> galaxiesToMoveX.addAll(galaxies.stream().filter(g -> g.x.intValue() > x).toList()));
        galaxiesToMoveX.forEach(g -> g.x = g.x.add(BigInteger.valueOf(factor)));

        var galaxiesToMoveY = new ArrayList<Galaxy>();
        missingLines.forEach(y -> galaxiesToMoveY.addAll(galaxies.stream().filter(g -> g.y.intValue() > y).toList()));
        galaxiesToMoveY.forEach(g -> g.y = g.y.add(BigInteger.valueOf(factor)));
    }

    static void calculateDistances() {
        galaxies.forEach(g -> galaxies.stream()
                .filter(dist -> !dist.x.equals(g.x) || !dist.y.equals(g.y))
                .forEach(dist -> g.distancesTo.put(dist, g.x.subtract(dist.x).abs().add(g.y.subtract(dist.y).abs()))));
    }

    static List<Galaxy> galaxies;
    static Long universeWidth;
    static Long universeHeight;

    static class Galaxy {
        BigInteger x; BigInteger y;
        Map<Galaxy, BigInteger> distancesTo;

        public Galaxy(BigInteger x, BigInteger y) {
            this.x = x;
            this.y = y;
            distancesTo = new HashMap<>();
        }
    }
}