package day6;

import utils.InputFileUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.LongStream;

import static utils.ConsoleUtils.ANSI_GREEN;
import static utils.ConsoleUtils.ANSI_RESET;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 6 🚀");
        var lines = InputFileUtils.extraction("day6/input.txt", "\n");
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage(lines) + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage(lines) + ANSI_RESET);
    }

    static Long firstStage(List<String> lines) {
        var times = Arrays.stream(lines.get(0).strip().split("\\s+")).filter(l -> !"Time:".equals(l)).mapToInt(Integer::parseInt).toArray();
        var records = Arrays.stream(lines.get(1).strip().split("\\s+")).filter(l -> !"Distance:".equals(l)).mapToInt(Integer::parseInt).toArray();
        var races = new ArrayList<Race>(times.length);
        for (int i = 0; i < times.length; i++) {
            races.add(i, new Race(times[i]));
        }
        for (int i = 0; i < records.length; i++) {
            races.get(i).record = records[i];
        }
        return races.stream()
                .mapToLong(race -> LongStream.range(0, race.time).filter(race::newRecord).count())
                .reduce(1, Math::multiplyExact);
    }

    static Long secondStage(List<String> lines) {
        var time = Long.parseLong( lines.get(0).split("Time:")[1].replaceAll("\\s", ""));
        var record = Long.parseLong( lines.get(1).split("Distance:")[1].replaceAll("\\s", ""));
        return LongStream.range(0, time).filter(hold -> hold * (time - hold) > record).count();
    }

    static class Race {
        long time; long record;
        public Race(long time) {
            this.time = time;
        }
        boolean newRecord(long holdTime) {
            return holdTime * (time - holdTime) > this.record;
        }
    }
}