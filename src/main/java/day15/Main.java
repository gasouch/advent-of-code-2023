package day15;

import utils.InputFileUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static utils.ConsoleUtils.ANSI_GREEN;
import static utils.ConsoleUtils.ANSI_RESET;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 15 🚀");
        var steps = InputFileUtils.extraction("day15/input.txt", ",");
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage(steps) + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage(steps) + ANSI_RESET);
    }

    static int firstStage(List<String> steps) {
        return steps.stream()
                .mapToInt(Main::runHashAlgo)
                .sum();
    }

    static int secondStage(List<String> steps) {
        steps.forEach(step -> {
            if (step.contains("-")) {
                var lensLabel = step.split("-")[0];
                var correctBox = runHashAlgo(lensLabel);
                if (boxes.containsKey(correctBox)) {
                    boxes.get(correctBox).lenses.remove(lensLabel);
                }
            } else if (step.contains("=")) {
                var lensLabel = step.split("=")[0];
                var correctBox = runHashAlgo(lensLabel);
                var lens = new Lens(Integer.parseInt(step.split("=")[1]));
                if (!boxes.containsKey(correctBox)) {
                    boxes.put(correctBox, new Box());
                }
                boxes.get(correctBox).lenses.put(lensLabel, lens);
            }
        });
        return boxes.entrySet().stream()
                .mapToInt(box -> {
                    var lenses = box.getValue().lenses;
                    String[] keys = lenses.keySet().toArray(new String[0]);
                    var power = 0;
                    for (int slot = 1; slot <= keys.length; slot++) {
                        power += (box.getKey() + 1) * slot * lenses.get(keys[slot - 1]).focalLength;
                    }
                    return power;
                }).sum();
    }

    private static Integer runHashAlgo(String sequence) {
        AtomicInteger start = new AtomicInteger();
        sequence.chars().forEach(c -> {
            start.addAndGet(c);
            start.set(start.get() * 17);
            start.set(start.get() % 256);
        });
        return start.get();
    }

    static Map<Integer, Box> boxes = new HashMap<>();

    static class Lens {
        Integer focalLength;

        public Lens(Integer focalLength) {
            this.focalLength = focalLength;
        }
    }

    static class Box {
        public LinkedHashMap<String, Lens> lenses = new LinkedHashMap<>();
    }
}