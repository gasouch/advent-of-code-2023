package day3;

import utils.InputFileUtils;

import java.awt.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 3 🚀");
        initData();
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static int firstStage() {
        return numbs.stream()
                .filter(Numb::isValidPart)
                .mapToInt(numb -> parseInt(numb.value))
                .sum();
    }

    private static int secondStage() {
        gears.forEach(Gear::calculateAdjacentParts);
        return gears.stream()
                .filter(g -> g.adjacentParts.size() == 2)
                .mapToInt(g -> parseInt(g.adjacentParts.get(0).value) * parseInt(g.adjacentParts.get(1).value))
                .sum();
    }

    private static void initData() throws FileNotFoundException {
        List<String> input = InputFileUtils.extraction("day3/input.txt", "\n");
        for (int y = 0; y < input.size(); y++) {
            String line = input.get(y);
            Matcher numbMatcher = Pattern.compile("\\d+").matcher(line);
            while (numbMatcher.find()) {
                numbs.add(new Numb(new Point(numbMatcher.start(), y), numbMatcher.group()));
            }
            Matcher symbMatcher = Pattern.compile("[\\/\\-=+!@#$%^&*(),?\":{}|<>]").matcher(line);
            while (symbMatcher.find()) {
                symbols.add(new Point(symbMatcher.start(), y));
            }
            Matcher gearMatcher = Pattern.compile("\\*").matcher(line);
            while (gearMatcher.find()) {
                gears.add(new Gear(new Point(gearMatcher.start(), y)));
            }
        }
    }

    static List<Numb> numbs = new ArrayList<>();
    static List<Point> symbols = new ArrayList<>();
    static List<Gear> gears = new ArrayList<>();

    private static class Numb {
        List<Point> positions;
        String value;

        public Numb(Point start, String value) {
            this.positions = new ArrayList<>();
            positions.add(start);
            for (int i = 1; i < value.length(); i++) {
                positions.add(new Point(start.x + i, start.y));
            }
            this.value = value;
        }

        boolean isValidPart() {
            AtomicBoolean validPart = new AtomicBoolean(false);
            positions.forEach(p ->
                    validPart.set(validPart.get() || symbols.stream().anyMatch(s -> Math.abs(s.y - p.y) <= 1 && Math.abs(s.x - p.x) <= 1))
            );
            return validPart.get();
        }

        boolean isNearGear(Gear g) {
            AtomicBoolean validPart = new AtomicBoolean(false);
            positions.forEach(p ->
                    validPart.set(validPart.get() || Math.abs(g.p.y - p.y) <= 1 && Math.abs(g.p.x - p.x) <= 1)
            );
            return validPart.get();
        }

    }

    private static class Gear {
        Point p; List<Numb> adjacentParts = new ArrayList<>();

        public Gear(Point p) {
            this.p = p;
        }

        // Warning : numbs must be init before
        void calculateAdjacentParts() {
            adjacentParts = numbs.stream().filter(n -> n.isNearGear(this)).toList();
        }
    }
}
