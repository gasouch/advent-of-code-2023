package day8;

import utils.InputFileUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static utils.ConsoleUtils.ANSI_GREEN;
import static utils.ConsoleUtils.ANSI_RESET;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 8 🚀");
        var lines = InputFileUtils.extraction("day8/input.txt", "\n\n");
        instructions = lines.get(0).chars().mapToObj(c -> Instruction.valueOf(String.valueOf((char) c))).toList();
        nodes = Arrays.stream(lines.get(1).split("\n"))
                .map(line -> new Node(line.substring(0, 3), line.substring(7, 10), line.substring(12, 15)))
                .toList();
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage() + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage() + ANSI_RESET);
    }

    static int firstStage() {
        int steps = 0;
        Node currentNode = getNodeFromCode("AAA");
        while (!currentNode.code.equals("ZZZ")) {
            for (Instruction instruction : instructions) {
                currentNode = getNodeFromCode(instruction == Instruction.L ? currentNode.left : currentNode.right);
                steps++;
            }
        }
        return steps;
    }

    static long secondStage() {
        var countsToZ = nodes.stream()
                .filter(node -> node.code.endsWith("A"))
                .map(node -> {
                    int steps = 0;
                    Node currentNode = node;
                    while (!currentNode.code.endsWith("Z")) {
                        for (Instruction instruction : instructions) {
                            currentNode = getNodeFromCode(instruction == Instruction.L ? currentNode.left : currentNode.right);
                            steps++;
                        }
                    }
                    return steps;
                })
                .collect(Collectors.toList());
        return calculateLcm(countsToZ);
    }

    /**
     * Calculate the Least Common Multiple of all the 6 paths depth
     */
    static long calculateLcm(List<Integer> countsToZ) {
        long currentLcm = countsToZ.getFirst();
        for (int i = 1; i < countsToZ.size(); i++) {
            currentLcm = currentLcm * (countsToZ.get(i) / gcd(currentLcm, countsToZ.get(i)));
        }
        return currentLcm;
    }

    static long gcd(long currentLcm, long countToZ) {
        while (countToZ > 0) {
            long temp = countToZ;
            countToZ = currentLcm % countToZ; // % is remainder
            currentLcm = temp;
        }
        return currentLcm;
    }

    static Node getNodeFromCode(String code) {
        return nodes.stream().filter((n -> code.equals(n.code))).findFirst().orElse(null);
    }

    static List<Instruction> instructions = new ArrayList<>();
    static List<Node> nodes = new ArrayList<>();
    enum Instruction { L, R }

    static class Node {
        String code; String left; String right;

        public Node(String code, String left, String right) {
            this.code = code;
            this.left = left;
            this.right = right;
        }
    }
}