package day5;

import utils.InputFileUtils;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

import static java.lang.Long.parseLong;
import static java.util.Arrays.stream;
import static utils.ConsoleUtils.*;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 5 🚀");
        var groups = InputFileUtils.extraction("day5/input.txt", "\n\n");
        initTransformers(groups);
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage(groups.get(0)) + ANSI_RESET);
        System.out.println("Starting stage 2...");
        var start = Instant.now();
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage(groups.get(0)) + ANSI_RESET);
        var diff = Duration.between(start, Instant.now());
        System.out.println(ANSI_YELLOW + "⏱️ Stage 2 took : " + String.format("%dh %02dm %02ds %02dms", diff.toHours(), diff.toMinutesPart(), diff.toSecondsPart(), diff.toMillisPart()) + ANSI_RESET);
    }

    private static Long firstStage(String seedsLine) {
        var seeds = stream(seedsLine.split("seeds:")[1].strip().split(" ")).map(Long::parseLong).toList();
        return seeds.stream().mapToLong(Main::computeSeed).min().orElseThrow();
    }

    private static Long secondStage(String seedsLine) throws IOException {
        var seedsMatcher = Pattern.compile("\\d+\\s\\d+").matcher(seedsLine.split("seeds:")[1].strip());
        var minLocation = Long.MAX_VALUE;
        var seedGroup = 1;
        while (seedsMatcher.find()) {
            var group = seedsMatcher.group();
            var firstSeed = Long.parseLong(group.split(" ")[0]);
            var range = Long.parseLong(group.split(" ")[1]);
            for (var currentSeed = firstSeed; currentSeed <= firstSeed + range; currentSeed++) {
                System.out.write(("\rSeeds group " + seedGroup + "/10 - Search progress : " + (100 * (currentSeed - firstSeed)) / range + " %").getBytes());
                var currentLocation = computeSeed(currentSeed);
                if (currentLocation < minLocation) {
                    minLocation = currentLocation;
                }
            }
            seedGroup++;
        }
        return minLocation;
    }

    static void initTransformers(List<String> groups) {
        seedToSoil = stream(groups.get(1).split("\n")).filter(l -> !"seed-to-soil map:".equals(l)).map(initMap()).toList();
        soilToFertilizer = stream(groups.get(2).split("\n")).filter(l -> !"soil-to-fertilizer map:".equals(l)).map(initMap()).toList();
        fertilizerToWater = stream(groups.get(3).split("\n")).filter(l -> !"fertilizer-to-water map:".equals(l)).map(initMap()).toList();
        waterToLight = stream(groups.get(4).split("\n")).filter(l -> !"water-to-light map:".equals(l)).map(initMap()).toList();
        lightToTemperature = stream(groups.get(5).split("\n")).filter(l -> !"light-to-temperature map:".equals(l)).map(initMap()).toList();
        temperatureToHumidity = stream(groups.get(6).split("\n")).filter(l -> !"temperature-to-humidity map:".equals(l)).map(initMap()).toList();
        humidityToLocation = stream(groups.get(7).split("\n")).filter(l -> !"humidity-to-location map:".equals(l)).map(initMap()).toList();
    }

    static long computeSeed(long seedNumber) {
        var soil = seedToSoil.stream().filter(t -> t.isEligibleForNumber(seedNumber)).findFirst().orElse(seedToSoil.get(0)).compute(seedNumber);
        var fertilizer = soilToFertilizer.stream().filter(t -> t.isEligibleForNumber(soil)).findFirst().orElse(soilToFertilizer.get(0)).compute(soil);
        var water = fertilizerToWater.stream().filter(t -> t.isEligibleForNumber(fertilizer)).findFirst().orElse(fertilizerToWater.get(0)).compute(fertilizer);
        var light = waterToLight.stream().filter(t -> t.isEligibleForNumber(water)).findFirst().orElse(waterToLight.get(0)).compute(water);
        var temperature = lightToTemperature.stream().filter(t -> t.isEligibleForNumber(light)).findFirst().orElse(lightToTemperature.get(0)).compute(light);
        var humidity = temperatureToHumidity.stream().filter(t -> t.isEligibleForNumber(temperature)).findFirst().orElse(temperatureToHumidity.get(0)).compute(temperature);
        return humidityToLocation.stream().filter(t -> t.isEligibleForNumber(humidity)).findFirst().orElse(humidityToLocation.get(0)).compute(humidity);
    }

    static Function<String, Transformer> initMap() {
        return l -> {
            var data = l.split(" ");
            return new Transformer(parseLong(data[0]), parseLong(data[1]), parseLong(data[2]));
        };
    }

    static class Transformer {
        long destinationRangeStart;
        long sourceRangeStart;
        long rangeLength;

        public Transformer(long destinationRangeStart, long sourceRangeStart, long rangeLength) {
            this.destinationRangeStart = destinationRangeStart;
            this.sourceRangeStart = sourceRangeStart;
            this.rangeLength = rangeLength;
        }

        Long compute(long source) {
            return isEligibleForNumber(source) ? destinationRangeStart + (source - sourceRangeStart) : source;
        }

        boolean isEligibleForNumber(long source) {
            return source >= sourceRangeStart && source <= sourceRangeStart + rangeLength;
        }
    }

    static List<Transformer> seedToSoil;
    static List<Transformer> soilToFertilizer;
    static List<Transformer> fertilizerToWater;
    static List<Transformer> waterToLight;
    static List<Transformer> lightToTemperature;
    static List<Transformer> temperatureToHumidity;
    static List<Transformer> humidityToLocation;
}
