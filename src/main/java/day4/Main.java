package day4;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 4 🚀");
        initData();
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    /**
     * 1 winning number  ->   1 pts
     * 2 winning number  ->   2 pts
     * 3 winning number  ->   4 pts
     * 4 winning number  ->   8 pts
     * 5 winning number  ->  16 pts
     * 6 winning number  ->  32 pts
     * 7 winning number  ->  64 pts
     * 8 winning number  ->  128 pts
     * 9 winning number  ->  256 pts
     * 10 winning number ->  512 pts
     */
    private static Integer firstStage() {
        return cards.stream().mapToInt(c -> c.score).sum();
    }

    private static Integer secondStage() {
        for (int i = 0; i < cards.size(); i++) {
            for (int j = i + 1; j <= i + cards.get(i).countWins(); j++) {
                cards.get(i).bonusCards.add(cards.get(j));
            }
        }
        cards.forEach(Main::computeCard);
        return totalCards.size();
    }

    static void computeCard(Card card) {
        totalCards.add(card);
        if (!card.bonusCards.isEmpty()) {
            card.bonusCards.forEach(Main::computeCard);
        }
    }

    private static void initData() throws FileNotFoundException {
        List<String> lines = InputFileUtils.extraction("day4/input.txt", "\n");
        cards = lines.stream()
                .map(line -> new Card(
                        Arrays.stream(line.substring(10, 39).strip().split("\\s+")).toList(),
                        Arrays.stream(line.substring(42).strip().split("\\s+")).toList()
                        // for the example : Arrays.stream(line.substring(8, 22).strip().split("\\s+")).toList(),
                        // for the example : Arrays.stream(line.substring(25).strip().split("\\s+")).toList()
                )).toList();
    }

    static List<Card> cards = new ArrayList<>();
    static List<Card> totalCards = new ArrayList<>();

    private static class Card {
        List<String> winningNumbers;
        List<String> numbers;
        Integer score;
        List<Card> bonusCards = new ArrayList<>();

        public Card(List<String> winningNumbers, List<String> numbers) {
            this.winningNumbers = winningNumbers;
            this.numbers = numbers;
            score = (int) Math.pow(2, countWins() - 1);
        }

        Long countWins() {
            return numbers.stream().filter(winningNumbers::contains).count();
        }
    }
}
