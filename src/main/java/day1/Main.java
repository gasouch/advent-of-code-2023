package day1;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 1 🚀");
        List<String> lines = InputFileUtils.extraction("day1/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage(lines));
        System.out.println("Response for stage 2 must be : " + secondStage(lines));
    }

    private static int firstStage(List<String> lines) {
        return lines.stream().mapToInt(line -> {
            Matcher m = Pattern.compile("[0-9]").matcher(line);
            List<String> numbers = new ArrayList<>();
            while (m.find()) {
                numbers.add(m.group());
            }
            return Integer.parseInt(numbers.getFirst() + numbers.getLast());
        }).sum();
    }

    private static int secondStage(List<String> lines) {
        return lines.stream().mapToInt(line -> {
            Matcher digitMatcher = Pattern.compile("[0-9]").matcher(line);
            Matcher textMatcher = Pattern.compile("(?=(one|two|three|four|five|six|seven|eight|nine))").matcher(line);
            TreeMap<Integer, String> numbers = new TreeMap<>();
            while (digitMatcher.find()) {
                numbers.put(digitMatcher.start(), digitMatcher.group());
            }
            while (textMatcher.find()) {
                numbers.put(textMatcher.start(), "" + TEXT_DIGITS.valueOf(textMatcher.group(1)).ordinal());
            }
            String firstNumber = numbers.entrySet().stream().findFirst().orElseThrow().getValue();
            String lastNumber = numbers.entrySet().stream().reduce((f, s) -> s).orElseThrow().getValue();
            return Integer.parseInt(firstNumber + lastNumber);
        }).sum();
    }

    enum TEXT_DIGITS {zero, one, two, three, four, five, six, seven, eight, nine}
}
