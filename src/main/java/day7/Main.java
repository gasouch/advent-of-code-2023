package day7;

import utils.InputFileUtils;

import java.io.IOException;
import java.util.*;

import static utils.ConsoleUtils.ANSI_GREEN;
import static utils.ConsoleUtils.ANSI_RESET;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 7 🚀");
        var lines = InputFileUtils.extraction("day7/input.txt", "\n");
        System.out.println(ANSI_GREEN + "Response for stage 1 must be : " + firstStage(lines) + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Response for stage 2 must be : " + secondStage(lines) + ANSI_RESET);
    }

    static Long firstStage(List<String> lines) {
        var hands = lines.stream()
                .map(l -> {
                    var originalCards = l.split("\\s")[0].chars().mapToObj(i -> CardPart1.valueOfLabel(String.valueOf((char) i))).toList();
                    return new Hand(
                            originalCards,
                            Long.parseLong(l.split("\\s")[1]),
                            calculateHandKindForPart1(originalCards)
                    );
                })
                .toList();
        return calculateOutput(hands);
    }

    static Long secondStage(List<String> lines) {
        var hands = lines.stream()
                .map(l -> {
                    var originalCards = l.split("\\s")[0].chars().mapToObj(i -> CardPart2.valueOfLabel(String.valueOf((char) i))).toList();
                    CardPart2 masterCard = calculateMasterCard(originalCards);
                    var cardsAfterJokerApplied = masterCard == null ? originalCards : new ArrayList<>(originalCards.stream().map(c -> c.equals(CardPart2.JACK) ? masterCard : c).toList());
                    return new Hand(
                            originalCards,
                            cardsAfterJokerApplied,
                            Long.parseLong(l.split("\\s")[1]),
                            calculateHandKindForPart2(cardsAfterJokerApplied)
                    );
                })
                .toList();
        return calculateOutput(hands);
    }

    static CardPart2 calculateMasterCard(List<CardPart2> cards) {
        Map<CardPart2, Integer> map = new HashMap<>();
        cards.stream().filter(c -> !c.equals(CardPart2.JACK)).forEach(c -> {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        });
        Comparator<Map.Entry<CardPart2, Integer>> comp = (Map.Entry<CardPart2, Integer> e1, Map.Entry<CardPart2, Integer> e2) -> {
            var byCount = Integer.compare(e1.getValue(), e2.getValue());
            if (byCount != 0) {
                return byCount;
            } else {
                return e1.getKey().compareTo(e2.getKey());
            }
        };
        return map.isEmpty() ? null : map.entrySet().stream().sorted(comp).toList().getLast().getKey();
    }

    static Kind calculateHandKindForPart1(List<CardPart1> cards) {
        var countDistinctCards = cards.stream().distinct().count();
        return switch ((int) countDistinctCards) {
            case 5 -> Kind.HIGH_CARD;
            case 4 -> Kind.ONE_PAIR;
            case 3 -> containsNOfaKindForPart1(cards, 3) ? Kind.THREE_OF_A_KIND : Kind.TWO_PAIR;
            case 2 -> containsNOfaKindForPart1(cards, 4) ? Kind.FOUR_OF_A_KIND : Kind.FULL;
            case 1 -> Kind.FIVE_OF_A_KIND;
            default -> throw new IllegalStateException("Unexpected value: " + countDistinctCards);
        };
    }

    static Kind calculateHandKindForPart2(List<CardPart2> cards) {
        var countDistinctCards = cards.stream().distinct().count();
        return switch ((int) countDistinctCards) {
            case 5 -> Kind.HIGH_CARD;
            case 4 -> Kind.ONE_PAIR;
            case 3 -> containsNOfaKindForPart2(cards, 3) ? Kind.THREE_OF_A_KIND : Kind.TWO_PAIR;
            case 2 -> containsNOfaKindForPart2(cards, 4) ? Kind.FOUR_OF_A_KIND : Kind.FULL;
            case 1 -> Kind.FIVE_OF_A_KIND;
            default -> throw new IllegalStateException("Unexpected value: " + countDistinctCards);
        };
    }


    static boolean containsNOfaKindForPart1(List<CardPart1> cards, int n) {
        Map<Object, Integer> map = new HashMap<>();
        cards.forEach(c -> {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        });
        return map.containsValue(n);
    }

    static boolean containsNOfaKindForPart2(List<CardPart2> cards, int n) {
        Map<Object, Integer> map = new HashMap<>();
        cards.forEach(c -> {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        });
        return map.containsValue(n);
    }

    private static Long calculateOutput(List<Hand> hands) {
        var sorted = hands.stream().sorted().toList();
        var count = 0L;
        for (int i = 0; i < sorted.size(); i++) {
            count += sorted.get(i).bid * (i + 1);
        }
        return count;
    }

    enum CardPart1 {
        TWO("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6"), SEVEN("7"), EIGHT("8"), NINE("9"), TEN("T"), JACK("J"), QUEEN("Q"), KING("K"), AS("A");
        final String value;

        CardPart1(String value) {
            this.value = value;
        }

        public static CardPart1 valueOfLabel(String label) {
            for (CardPart1 e : values()) {
                if (e.value.equals(label)) {
                    return e;
                }
            }
            return null;
        }
    }

    enum CardPart2 {
        JACK("J"), TWO("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6"), SEVEN("7"), EIGHT("8"), NINE("9"), TEN("T"), QUEEN("Q"), KING("K"), AS("A");
        final String value;

        CardPart2(String value) {
            this.value = value;
        }

        public static CardPart2 valueOfLabel(String label) {
            for (CardPart2 e : values()) {
                if (e.value.equals(label)) {
                    return e;
                }
            }
            return null;
        }
    }

    enum Kind {
        HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_A_KIND, FULL, FOUR_OF_A_KIND, FIVE_OF_A_KIND
    }

    static class Hand implements Comparable<Hand> {
        List<CardPart1> part1Cards;
        List<CardPart2> originalCards;
        List<CardPart2> cardsAfterJokerApplied;
        Kind kind;
        Long bid;

        public Hand(List<CardPart1> cards, Long bid, Kind kind) {
            this.part1Cards = cards;
            this.bid = bid;
            this.kind = kind;
        }

        public Hand(List<CardPart2> originalCards, List<CardPart2> cardsAfterJokerApplied, Long bid, Kind kind) {
            this.bid = bid;
            this.originalCards = originalCards;
            this.cardsAfterJokerApplied = cardsAfterJokerApplied;
            this.kind = kind;
        }

        @Override
        public int compareTo(Hand other) {
            if (part1Cards != null) { // part 1
                if (kind.ordinal() == other.kind.ordinal()) {
                    int compare = 0;
                    for (int i = 0; compare == 0 && i < part1Cards.size(); i++) {
                        compare = part1Cards.get(i).compareTo(other.part1Cards.get(i));
                    }
                    return compare;
                } else {
                    return kind.compareTo(other.kind);
                }
            } else { // part 2
                if (kind.ordinal() == other.kind.ordinal()) {
                    int compare = 0;
                    for (int i = 0; compare == 0 && i < originalCards.size(); i++) {
                        compare = originalCards.get(i).compareTo(other.originalCards.get(i));
                    }
                    return compare;
                } else {
                    return kind.compareTo(other.kind);
                }
            }
        }
    }
}