package day2;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;
import static java.util.Comparator.comparing;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 2 🚀");
        List<String> lines = InputFileUtils.extraction("day2/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage(lines));
        System.out.println("Response for stage 2 must be : " + secondStage(lines));
    }

    private static int firstStage(List<String> lines) {
        return lines.stream()
                .map(Main::initGame)
                .filter(g-> g.hands.stream().allMatch(h -> h.countGreen <= greenLimit && h.countRed <= redLimit && h.countBlue <= blueLimit))
                .mapToInt(game -> game.id)
                .sum();
    }

    private static int secondStage(List<String> lines) {
        return lines.stream()
                .map(Main::initGame)
                .mapToInt(game -> game.minGreen * game.minRed * game.minBlue)
                .sum();
    }

    private static Game initGame(String line) {
        Matcher gameIdMatcher = gameIdPattern.matcher(line);
        if (!gameIdMatcher.find()) {
            throw new RuntimeException("pas d'bol mon pote...");
        }
        List<Game.Hand> hands = Arrays.stream(line.split(":")[1].split(";"))
                .map(hand -> {
                    Matcher greenCountMatcher = greenCountPattern.matcher(hand);
                    Matcher redCountMatcher = redCountPattern.matcher(hand);
                    Matcher blueCountMatcher = blueCountPattern.matcher(hand);
                    return new Game.Hand(
                            greenCountMatcher.find() ? parseInt(greenCountMatcher.group(1)) : 0,
                            redCountMatcher.find() ? parseInt(redCountMatcher.group(1)) : 0,
                            blueCountMatcher.find() ? parseInt(blueCountMatcher.group(1)) : 0
                    );
                }).toList();
        int minGreen = hands.stream().max(comparing(h -> h.countGreen)).map(h -> h.countGreen).orElse(0);
        int minRed = hands.stream().max(comparing(h -> h.countRed)).map(h -> h.countRed).orElse(0);
        int minBlue = hands.stream().max(comparing(h -> h.countBlue)).map(h -> h.countBlue).orElse(0);
        return new Game(parseInt(gameIdMatcher.group(1)), hands, minGreen, minRed, minBlue);
    }

    static class Game {
        int id;
        List<Hand> hands;
        int minGreen; int minRed; int minBlue;
        public Game(int id, List<Hand> hands, int minGreen, int minRed, int minBlue) {
            this.id = id;
            this.hands = hands;
            this.minGreen = minGreen;
            this.minRed = minRed;
            this.minBlue = minBlue;
        }

        static class Hand {
            int countGreen; int countRed; int countBlue;
            public Hand(int countGreen, int countRed, int countBlue) {
                this.countGreen = countGreen;
                this.countRed = countRed;
                this.countBlue = countBlue;
            }
        }

    }

    static int greenLimit = 13;
    static int redLimit = 12;
    static int blueLimit = 14;
    static Pattern gameIdPattern = Pattern.compile("^Game (\\d+)+:.*$");
    static Pattern greenCountPattern = Pattern.compile("(\\d+)+ green");
    static Pattern redCountPattern = Pattern.compile("(\\d+)+ red");
    static Pattern blueCountPattern = Pattern.compile("(\\d+)+ blue");

}
